QUIT = "quit"
RELOAD = "reload"
CORRECT = "correct"
PRESENT = "present"
ABSENT = "absent"
HELP = "help"

HELP_TEXT = """
Help page of wordler for the ignorant.

    All instructions must be passed in space-separated pairs: [command] [argument]
    Mutiple comman-separated instructions can be passed:
                [command 1] [argument 1], [command 2] [argument 2], [command 3] [argument 3]

    absent <*args>    : Eliminates words containing letters in *args. Example-
                                absent dieu
                            will eliminate all words containing the letters d, i, e and u.
                        Use for grey letters.

    present <i><l>    : Shortlists all words the has the letter <l> but not in the <i>th index
                            Position starts from 0 and ends at 4. Example-
                                present 1f
                            will shortlist all words containing the letter `f` but exclude words like `after`.
                        Use for yellow letters.

    correct <i><l>    : Shortlists all words that has the letter <l> in the <i>th index. Example-
                                correct 1f
                            will also shortlist words like `after`, `often`, `offer`.
                        Use for green letters.

    reload            : Use to reload the wordlist to start afresh. Will nullify all previous operations.

    help              : Use to access this page.

    quit              : Use to exit wordler.

Commands can be <tab> auto-completed. Previous commands can be accessed using the <UP ARROW> key.
"""

COMMANDS = [QUIT, RELOAD, CORRECT, PRESENT, ABSENT, HELP]
