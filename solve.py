#!/opt/homebrew/bin/python3
import readline

from completer import SimpleCompleter
from constants import COMMANDS
from wordler import run_wordler


readline.set_completer(SimpleCompleter(COMMANDS).complete)
readline.parse_and_bind('tab: complete')


if __name__=="__main__":
    run_wordler()
