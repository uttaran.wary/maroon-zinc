from constants import (
    PRESENT,
    ABSENT,
    CORRECT
)


def verify_user_input(text):
    input_instructions = [input_instruction.split() for input_instruction in text.split(',')]

    instructions_list = []

    for instruction in input_instructions:
        command = instruction[0]
        if not verify_input_command(command):
            print(f"Invalid command: {command}")
            return False, None

        if len(instruction) < 2:
            print(f"Insufficient argument for command: {command}!")
            return False, None
        
        if len(instruction) > 2:
            print(f"Pass only 1 argument for each command: {command}")
            return False, None

        argument = instruction[1]
        if not verify_input_argument(command, argument):
            return False, None

        instructions_list.append((command, argument))

    return True, instructions_list


def verify_input_command(command:str):
    if command not in [PRESENT, ABSENT, CORRECT]:
        return False
    return True


def verify_letters(letters:str):
    for letter in letters:
        if not letter.isalpha():
            return False
    return True


def verify_position_letter_pairs(arg:str):
    if not len(arg) == 2:
        return False
    if not arg[0].isdigit():
        return False
    if not arg[1].isalpha():
        return False
    return True


def verify_input_argument(command, argument):

    if command == ABSENT:
        if not verify_letters(argument):
            print("Invalid input letter[s]..")
            return False

    if command in [CORRECT, PRESENT]:
        if not verify_position_letter_pairs(argument):
            print(f"Invalid arguments for *{command}*..")
            return False

    return True


def _verify_position_letter_pairs_experimente(arg:str):
    length_of_arg = len(arg)
    if length_of_arg % 2 != 0:
        print(f"Length of argument cannot be an odd number: {length_of_arg}")
        return False

    flag = True
    for char in arg:
        if flag:
            if not char.isdigit():
                print(f"Even indexed character has to be a number: {char}")
                return False
            else:
                if int(char) > 4:
                    print(f"Index cannot be larger than 4: {char}")
                    return False
        else:
            if not char.isalpha():
                print(f"Odd indexed char has to be an alphabet: {char}")
                return False
        flag = not flag

    return True
