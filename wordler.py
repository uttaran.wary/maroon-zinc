from constants import (
    CORRECT,
    QUIT,
    RELOAD,
    ABSENT,
    PRESENT,
    HELP,
    HELP_TEXT
)

from verifiers import verify_user_input


def load_words_from_file():
    with open("words.txt", 'r') as f:
        words = f.read().splitlines()
    return words


def absent(arguments, words):
    shortlist = []

    for word in words:
        letter_not_in_word = True
        for letter in word:
            if letter in arguments:
                letter_not_in_word = False
        if letter_not_in_word:
            shortlist.append(word)

    return shortlist


def present(argument, words):
    shortlist = []
    position = int(argument[0])
    letter = argument[1]

    for word in words:
        if letter in word and word[position] != letter:
            shortlist.append(word)
    
    return shortlist


def correct(argument, words):
    shortlist = []
    position = int(argument[0])
    letter = argument[1]

    for word in words:
        if word[position] == letter:
            shortlist.append(word)

    return shortlist


def run_evaluator(instructions, words):
    for instruction in instructions:

        command = instruction[0]
        argument = instruction[1]

        if command == ABSENT:
            words = absent(argument, words)

        if command == PRESENT:
            words = present(argument, words)

        if command == CORRECT:
            words = correct(argument, words)

    return words


def is_quit_entered(text):
    length_of_quit = len(QUIT)

    if len(text) >= length_of_quit:
        quit_text_holder = text[:4]
        if quit_text_holder == QUIT:
            if len(text) > length_of_quit:
                print(f"Quitting with arguments: {text[4:]}")
            print("\nBye bye..!")
            return True
    return False


def is_help_entered(text):
    length_of_help = len(HELP)

    if len(text) >= length_of_help:
        help_text_holder = text[:4]
        if help_text_holder == HELP:
            if len(text) > length_of_help:
                print(f"Helping with arguments: {text[4:]}")
            return True
    return False


def help_the_ignorant():
    print(HELP_TEXT)


def run_wordler():
    print("Starting wordle solver...")
    help_the_ignorant()

    words = load_words_from_file()

    while True:
        text = input("command: ")

        if not text: continue

        elif is_quit_entered(text): exit()

        elif is_help_entered(text):
            help_the_ignorant()
            continue

        elif text == RELOAD:
            words = load_words_from_file()
            print("\nWordlist reloaded.!")
            continue
        
        else:
            is_verified, instructions = verify_user_input(text)
            if not is_verified:
                continue

            words = run_evaluator(instructions, words)

            print()
            print(words)
            print()


"""
commands: present, absent, correct, refresh, quit
"""
